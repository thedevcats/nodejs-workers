import { mapCardsToOld } from './services/old-parser.service';
import { mapOldCardsToNew } from './services/cards-mapper.service';
import { downloadBulkData } from './services/download.service';

export const start = async () => {
  const hrstart = process.hrtime();

  console.log('Step 1 - Download bulk data');
  // await downloadBulkData();

  console.log('Step 2 - Map cards to old format');
  let _oldCards = await mapCardsToOld();
  const oldCards = _oldCards.filter((d) => d.tcgplayer_id);

  console.log('Step 3 - Map cards and prices to new format');
  const { cards, prices } = await mapOldCardsToNew(oldCards);
  console.log(`DONE - Mapped ${cards.length} cards.`);
  console.log(`DONE - Mapped ${prices.length} prices.`);

  const hrend = process.hrtime(hrstart);
  console.info('Execution time (hr): %ds %dms', hrend[0], hrend[1] / 1000000);
};
