export interface NewPrice {
  created_on: number;
  card_id: number;
  set_id: string;
  is_foil: boolean;
  usd: string;
  usd_foil: string;
  eur: string;
  tix: string;
}
