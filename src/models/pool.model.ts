import { PoolWokerPaths } from '../worker-paths';

export interface PoolWorker {
  id: string;
  path: PoolWokerPaths;
  data: { poolDataArr: Array<any>; [name: string]: any };
}
