export interface OldCard {
  object: string;
  id: string;
  oracle_id: string;
  multiverse_ids: Array<number>;
  mtgo_id: number;
  mtgo_foil_id: number;
  tcgplayer_id: number;
  name: string;
  lang: string;
  released_at: Date;
  uri: string;
  scryfall_uri: string;
  layout: string;
  highres_image: boolean;
  image_uris: OldCardImageUris;
  mana_cost: string;
  cmc: number;
  type_line: string;
  oracle_text: string;
  power: string;
  toughness: string;
  colors: Array<string>;
  color_identity: Array<string>;
  keywords: Array<string>;
  legalities: OldCardLegalities;
  games: Array<string>;
  reserved: boolean;
  foil: boolean;
  nonfoil: boolean;
  oversized: boolean;
  promo: boolean;
  reprint: boolean;
  variation: boolean;
  set: string;
  set_name: string;
  set_type: string;
  set_uri: string;
  set_search_uri: string;
  scryfall_set_uri: string;
  rulings_uri: string;
  prints_search_uri: string;
  collector_number: string;
  digital: boolean;
  rarity: string;
  flavor_text: string;
  card_back_id: string;
  artist: string;
  artist_ids: Array<string>;
  illustration_id: string;
  border_color: string;
  frame: string;
  full_art: boolean;
  textless: boolean;
  booster: boolean;
  story_spotlight: boolean;
  promo_types: Array<string>;
  edhrec_rank: number;
  preview: OldCardPreview;
  prices: OldPrices;
  related_uris: OldCardRelatedUris;
}

export interface OldPrices {
  usd: string;
  usd_foil: string;
  eur: string;
  tix: string;
}

export interface OldCardImageUris {
  small: string;
  normal: string;
  large: string;
  png: string;
  art_crop: string;
  border_crop: string;
}

export interface OldCardLegalities {
  standard: string;
  future: string;
  historic: string;
  pioneer: string;
  modern: string;
  legacy: string;
  pauper: string;
  vintage: string;
  penny: string;
  commander: string;
  brawl: string;
  duel: string;
  oldschool: string;
}

export interface OldCardPreview {
  source: string;
  source_uri: string;
  previewed_at: Date;
}

export interface OldCardRelatedUris {
  tcgplayer_decks: string;
  edhrec: string;
  mtgtop8: string;
}
