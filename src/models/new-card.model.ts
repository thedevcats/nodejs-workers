import { NewPrice } from './new-price.model';

export interface Card {
  artist: string;
  borderColor: string;
  booster: false;
  cmc: number;
  colorIdentity: Array<string>;
  colors: Array<string>;
  keywords: Array<string>;
  flavorText: string;
  foil: boolean;
  id: number;
  imageUrl: string;
  setImageUrl: string;
  lang: string;
  layout: string;
  legalities: Legalities;
  manaCost: string;
  multiverseIds: Array<number>;
  name: string;
  oracleText: string;
  prices: NewPrice;
  preview: Preview;
  power: number;
  promo: boolean;
  rarity: string;
  edhrecRank: number;
  edhrecURI: string;
  releasedAt: number;
  reprint: boolean;
  set: string;
  setName: string;
  setType: string;
  toughness: number;
  textless: false;
  type: string;
}

export interface Legalities {
  standard: string;
  modern: string;
  legacy: string;
  vintage: string;
  commander: string;
  brawl: string;
  duel: string;
}

export interface Preview {
  source: string;
  source_uri: string;
  previewed_at: Date;
}

export interface RelatedUris {
  tcgplayer_decks: string;
  edhrec: string;
  mtgtop8: string;
}
