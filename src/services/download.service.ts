import axios from 'axios';
import fs from 'fs';

const BULK_URL = 'https://api.scryfall.com/bulk-data';

const tPath = 'dist';
const cardsFileDir = `${tPath}/assets/cards/`;
const cardsFileUri = `${cardsFileDir}bulk_data.json`;

export const downloadBulkData = async () => {
  console.log('File download started.', cardsFileDir);
  const bulkDataInfo = await (await axios.get(BULK_URL)).data;
  const allCardsUrl = bulkDataInfo.data[2].download_uri;

  await axios
    .get(allCardsUrl, {
      responseType: 'json',
      headers: {
        'Content-Encoding': 'gzip',
      },
    })
    .then(async (response) => {
      console.log('File download complete.');
      fs.writeFileSync(cardsFileUri, JSON.stringify(response.data, null, 2), {
        flag: 'w',
      });
    })
    .catch((error) => {
      console.log(error);
    });
};
