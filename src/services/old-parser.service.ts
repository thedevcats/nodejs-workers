import fs from 'fs';
import { OldCard } from '../models/old-card.model';

const tPath = 'dist';

export const mapCardsToOld = async () => {
  let rawData = fs.readFileSync(`${tPath}/assets/cards/bulk_data.json`);

  return JSON.parse(rawData.toString()) as Array<OldCard>;
};
