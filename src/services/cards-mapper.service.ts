import { createWorkerPool } from '../pool/pool';

const THREADS = 4;

export const mapOldCardsToNew = async (oldCards: Array<any>): Promise<any> => {
  const result: any = await createWorkerPool('oldCardsMapper', THREADS, {
    poolDataArr: oldCards,
  });

  const cards = [];
  const prices = [];
  for (let i = 0; i < result.length; i++) {
    cards.push(...result[i].cards);
    prices.push(...result[i].prices);
  }

  return { cards, prices };
};
