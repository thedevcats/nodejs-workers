import { workerData, parentPort } from 'worker_threads';
import { Card } from '../models/new-card.model';
import { uniqBy as _uniqBy, random as _random } from 'lodash';
import { NewPrice } from '../models/new-price.model';

export const removeDuplicateCards = async (
  cardsData: Array<Card>
): Promise<any> => {
  const uniqueCards = _uniqBy(cardsData, 'id');
  return uniqueCards;
};

(async () => {
  let { poolDataArr } = workerData.data;

  const cardsData: Array<Card> = [];
  const pricesData: Array<NewPrice> = [];
  for (var i = 0; i < poolDataArr.length; i++) {
    const newPrice: NewPrice = {
      card_id: poolDataArr[i].tcgplayer_id,
      created_on: new Date().valueOf(),
      set_id: poolDataArr[i].set,
      is_foil: poolDataArr[i].foil,
      usd: poolDataArr[i].prices.usd,
      eur: poolDataArr[i].prices.eur,
      usd_foil: poolDataArr[i].prices.usd_foil,
      tix: poolDataArr[i].prices.tix,
    };

    cardsData.push({
      artist: poolDataArr[i].artist,
      borderColor: poolDataArr[i].border_color,
      booster: poolDataArr[i].booster,
      cmc: poolDataArr[i].cmc,
      colorIdentity: poolDataArr[i].color_identity,
      colors: poolDataArr[i].colors,
      keywords: poolDataArr[i].keywords,
      flavorText: poolDataArr[i].flavor_text
        ? poolDataArr[i].flavor_text
        : null,
      foil: poolDataArr[i].foil,
      id: poolDataArr[i].tcgplayer_id,
      imageUrl: poolDataArr[i].image_uris
        ? poolDataArr[i].image_uris.normal
        : '',
      setImageUrl: `https://img.scryfall.com/sets/${poolDataArr[i].set}.svg`,
      lang: poolDataArr[i].lang,
      layout: poolDataArr[i].layout,
      legalities: {
        standard: poolDataArr[i].legalities.standard,
        modern: poolDataArr[i].legalities.modern,
        legacy: poolDataArr[i].legalities.legacy,
        vintage: poolDataArr[i].legalities.vintage,
        commander: poolDataArr[i].legalities.commander,
        brawl: poolDataArr[i].legalities.brawl,
        duel: poolDataArr[i].legalities.duel,
      },
      manaCost: poolDataArr[i].mana_cost,
      multiverseIds: poolDataArr[i].multiverse_ids,
      name: poolDataArr[i].name,
      oracleText: poolDataArr[i].oracle_text,
      prices: newPrice,
      preview: poolDataArr[i].preview ? poolDataArr[i].preview : null,
      power: poolDataArr[i].power ? +poolDataArr[i].power : null,
      promo: poolDataArr[i].promo,
      rarity: poolDataArr[i].rarity,
      edhrecRank: poolDataArr[i].edhrec_rank,
      edhrecURI: poolDataArr[i].related_uris.edhrec,
      releasedAt: new Date(poolDataArr[i].released_at).valueOf(),
      reprint: poolDataArr[i].reprint,
      set: poolDataArr[i].set,
      setName: poolDataArr[i].set_name,
      setType: poolDataArr[i].set_type,
      toughness: poolDataArr[i].toughness ? +poolDataArr[i].toughness : null,
      textless: poolDataArr[i].textless,
      type: poolDataArr[i].type_line,
    } as Card);

    pricesData.push(newPrice);
  }

  const newCardsData = await removeDuplicateCards(cardsData);

  const msg = {
    ok: true,
    data: { cards: newCardsData, prices: pricesData },
  };
  parentPort.postMessage(msg);
})();
