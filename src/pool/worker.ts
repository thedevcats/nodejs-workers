import { Worker } from 'worker_threads';

const WORKER_TIMEOUT = 10000; // 10s

export const createAndRunWorker = async (id: any, workerData: any) => {
  return new Promise((resolve, reject) => {
    console.log('\x1b[36m%s\x1b[0m', `*** Worker [${id}] created ***`);
    const worker = new Worker(workerData.path, {
      workerData,
    });

    worker.once('error', (msg) => {
      worker.terminate();
    });

    worker.once('exit', async (code) => {
      if (code === 1) {
        console.log(
          '\x1b[31m%s\x1b[0m',
          `*** Worker [${id}] had issues. \nRecreating Worker [${id}] in ${(
            (WORKER_TIMEOUT % 60000) /
            1000
          ).toFixed(0)}s ***`
        );
        setTimeout(async () => {
          resolve(await createAndRunWorker(id, workerData));
        }, WORKER_TIMEOUT);
      }
    });

    worker.once('message', (msg) => {
      if (msg.ok && msg.data) {
        console.log('\x1b[32m%s\x1b[0m', `*** Worker [${id}] finished ***`);
        resolve(msg.data);
      } else {
        worker.terminate();
      }
    });
  });
};
