import { createAndRunWorker } from './worker';
import { PoolWorker } from '../models/pool.model';
import { PoolWokerPaths } from '../worker-paths';
import { extractChunks } from '../services/chunk-extractor';

export const createWorkerPool = async (
  poolWorkerType: string,
  poolWorkerThreads: number,
  poolData: PoolWorker['data']
) => {
  return new Promise(async (resolve, reject) => {
    console.time(`*** Pool [${poolWorkerType}]`);
    const pool: Array<any> = [];
    const { poolDataArr, ...poolExtraData } = poolData;
    const poolDataChunks = await extractChunks(
      poolDataArr,
      poolDataArr.length / poolWorkerThreads
    );
    for (let i = 0; i < poolWorkerThreads; i++) {
      const workerId = `${poolWorkerType}-${i + 1}`;
      const worker: PoolWorker = {
        id: workerId,
        path: PoolWokerPaths[poolWorkerType],
        data: { poolDataArr: poolDataChunks[i], ...poolExtraData },
      };
      pool.push(createAndRunWorker(workerId, worker));
    }

    return await Promise.all([...pool])
      .then((array: any) => {
        console.timeEnd(`*** Pool [${poolWorkerType}]`);
        const used = process.memoryUsage().heapUsed / 1024 / 1024;
        console.log(
          `*** Pool [${poolWorkerType}] used approximately ${used} MB.`
        );
        if (array) {
          const result = array.flat();
          resolve(result);
        }
        resolve();
      })
      .catch((err) => {
        console.log(`Pool [${poolWorkerType}] has encountered an error. `, err);
        reject();
      });
  });
};
