import { start } from './main';

(async () => {
  await start();
})();
